<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div style="display: grid; place-items: center;">
            <img src="images/undraw_Login_re_4vu2.png" alt="" style="width: 70%;">
        </div>
        <h2>Login </h2>
        <div class="row">
            <div class="col-md-6">

                <form action="check.php" method="post">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input required type="text" name="name" class="form-control" aria-describedby="emailHelp"
                            placeholder="Enter username">
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input required type="password" name="password" class="form-control"
                            aria-describedby="emailHelp" placeholder="Enter password">
                    </div>


                    <button type="submit" class="btn btn-primary">Login</button>
                </form>

            </div>
            <div class="col-md-6">

            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
                <?php
                        if (isset($_GET['error'])){
                            ?>
                <div class="alert alert-danger" role="alert">
                    Invalid Username or Password!
                </div>
                <?php
                        }
                    ?>
            </div>
        </div>

    </div>

</body>

</html>